require('dotenv').config();
const path = require('path');
const Dotenv = require('dotenv-webpack');
const withImages = require('next-images');

module.exports = withImages(
    {
        exclude: path.resolve(__dirname, 'svg'),
        useFileSystemPublicRoutes: false,
        webpack: (config, options) => {
            config.plugins = [
                ...config.plugins,
                new Dotenv({
                    path: path.join(__dirname, '.env'),
                    systemvars: true
                })
            ];
            config.module.rules.push(
                {
                    test: /\.svg$/,
                    exclude: /(node_modules|public)/,
                    loader: 'svg-react-loader',
                },
            );

            return config;
        }
    });