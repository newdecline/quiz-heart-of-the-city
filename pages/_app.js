import App, {Container} from 'next/app';
import React from 'react';
import {GlobalStyle} from "../globalStyles";
import {MainLayout} from "../components/layouts/MainLayout";
import {Provider} from 'mobx-react'

import initializeStore from '../stores/stores';

class MyMobxApp extends App {
    static async getInitialProps(appContext) {
        const mobxStore = initializeStore();
        appContext.ctx.mobxStore = mobxStore;
        const appProps = await App.getInitialProps(appContext);
        return {
            ...appProps,
            initialMobxState: mobxStore,
        };
    }

    constructor(props) {
        super(props);
        const isServer = typeof window === 'undefined';
        this.mobxStore = isServer ? props.initialMobxState : initializeStore(props.initialMobxState);
    }

    render() {
        const {Component, pageProps} = this.props;

        return (
            <Provider {...this.mobxStore}>
                <Container>
                    <GlobalStyle/>
                    <MainLayout>
                        <Component {...pageProps} />
                    </MainLayout>
                </Container>
            </Provider>
        )
    }
}

export default MyMobxApp;