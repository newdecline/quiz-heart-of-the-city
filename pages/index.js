import React, {useRef, useEffect} from 'react';
import ArrowSmallIcon from "../svg/arrow-small.svg";
import DotIcon from "../svg/dot.svg";
import {data as howItWorksData} from "../fakeData/howItWorks";
import {QuizPopup} from "../components/QuizPopup/QuizPopup";
import {inject, observer} from 'mobx-react';
import scrollIntoView from "scroll-into-view";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import {socialList} from "../fakeData/socialList";
import {StyledFirstSection} from "../components/StyledSections/StyledFirstSection";
import {StyledHowItWorksSection} from "../components/StyledSections/StyledHowItWorksSection";
import {StyledThirdSection} from "../components/StyledSections/ThirdSectionStyled";
import {StyledOffersSection} from "../components/StyledSections/StyledOffersSection";
import {StyledTakeSurveyNow} from "../components/StyledSections/StyledTakeSurveyNow";

export default inject('quizStore')(observer(props => {
  const {
    quizStore,
    quizStore: {isOpenQuizPopup}
  } = props;

  const howItWorksRef = useRef(null);

  const handleClickMoreBtn = () => {
    scrollIntoView(howItWorksRef.current, {
      time: 500,
      align: {
        top: 0,
        topOffset: 30
      }
    })
  };

  const handleClickStartQuizBtn = () => {
    const targetScroll = document.getElementById('main');
    scrollIntoView(targetScroll, {
      time: 500,
      align: {
        top: 0,
        topOffset: 30
      }
    }, () => {
      quizStore.isOpenQuizPopup = true
    });
  };

  useEffect(() => {
    if (process.browser) {
      const $quizPopupWrapper = document.querySelector('.quiz-popup');
      const $consultantWrapper = document.querySelector('.b24-widget-button-position-bottom-left');

      if (quizStore.isOpenQuizPopup) {
        disablePageScroll($quizPopupWrapper);

        if (window.matchMedia("(max-width: 1023px)").matches) {
          $consultantWrapper && $consultantWrapper.classList.add('hidden-consultant-when-open-menu');
        }
      } else {
        enablePageScroll($quizPopupWrapper);

        if (window.matchMedia("(max-width: 1023px)").matches) {
          $consultantWrapper && $consultantWrapper.classList.remove('hidden-consultant-when-open-menu');
        }
      }
    }
  }, [quizStore.isOpenQuizPopup]);

  return (
    <>
      <StyledFirstSection isOpenQuizPopup={isOpenQuizPopup}>
        <div id='main' className="content-block">
          <div className="content-block__text">
            <h1 className="title">Все новостройки Москвы: цены и скидки от застройщиков</h1>
            <h2 className="subtitle">Экономим ваше время, бесплатно&nbsp;подбираем квартиры по&nbsp;индивидуальным
              пожеланиям</h2>
          </div>

          <button onClick={handleClickStartQuizBtn} className='start-quiz'>пройти опрос</button>
          <button onClick={handleClickMoreBtn} className="more"><span
            className='more__text'>Узнать больше об услуге</span><ArrowSmallIcon/></button>
        </div>
        <div className="picture-block">
          <button onClick={handleClickMoreBtn} className="more">
            <span className='more__text'>Узнать больше об услуге</span>
            <ArrowSmallIcon/>
          </button>
          <ul className="social-list">
            {
              socialList.map(item => (
                <li key={item.href} className="social-list__item">
                  <a rel='noreferrer noopener' target='_blank' href={item.href}>{item.icon}</a>
                </li>
              ))
            }
          </ul>
        </div>
      </StyledFirstSection>
      <StyledHowItWorksSection id='how-it-works' ref={howItWorksRef}>
        <h3 className="title">Как это работает?</h3>
        <h3 className="subtitle">Пройдите опрос всего за 3 минуты и получите бесплатную подборку&nbsp;квартир по
          вашим пожеланиям</h3>
        <div className="steps">
          {
            howItWorksData.map(item => (
              <div key={item.number} className="steps__item-wrap">
                <div className="dots-container">
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                  <DotIcon className='dot'/>
                </div>
                <div className={`steps__item item-${item.number}`}>
                  <div className={`item-number item-number-${item.number}`}>{item.number}</div>
                  <div className="item-title">{item.title}</div>
                  <div className="item-subtitle">{item.subtitle}</div>
                </div>
              </div>
            ))
          }
        </div>
      </StyledHowItWorksSection>
      <StyledThirdSection>
        <div className="content">
          <p className="text">Даём только проверенную информацию о жилых комплексах.
            <br/> Гарантируем достоверность всех данных.</p>
        </div>
        <div className="picture"/>
      </StyledThirdSection>
      <StyledOffersSection>
        <div id='you-will-get' className="you-will-get">
          <div className="title">Вы получите:</div>
          <ul className="list">
            <li className="list__item">Подборку подходящих вам квартир</li>
            <li className="list__item">Полную презентацию жилых комплексов</li>
            <li className="list__item">Мобильную консультацию менеджера в удобном вам месте, например в
              вашем офисе или кафе
            </li>
            <li className="list__item">Экспертизу документов застройщика перед покупкой</li>
            <li className="list__item">Юридическое сопровождение покупки</li>
            <li className="list__item">Профессиональную финансовую консультацию</li>
            <li className="list__item">Оформление ипотечного кредита</li>
            <li className="list__item">Помощь в технической приёмке квартиры</li>
          </ul>
        </div>
        <div id='why-free' className="why-free">
          <div className="title">Почему это бесплатно?</div>
          <ul className="list">
            <li className="list__item">Застройщик нанимает компанию "Сердце города" для поиска покупателей
              квартир
            </li>
            <li className="list__item">Оплату за услуги осуществляет застройщик</li>
            <li className="list__item">Стоимость и условия покупки недвижимости не отличаются от условий,
              которые предлагает застройщик
            </li>
          </ul>
        </div>
      </StyledOffersSection>
      <StyledTakeSurveyNow>
        <div className="content">
          <h3 className="title">Пройдите опрос сейчас</h3>
          <p className='subtitle'>и уже через 3 часа получите подборку квартир по индивидуальным
            пожеланиям</p>
          <button onClick={handleClickStartQuizBtn} className='start-quiz'>пройти опрос</button>
        </div>
        <div className="picture"/>
      </StyledTakeSurveyNow>
      {isOpenQuizPopup && <QuizPopup className='quiz-popup'/>}
    </>
  )
}));