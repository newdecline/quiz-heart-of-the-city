const getRandomCeil = (min, max) => {
    return Math.ceil(Math.random() * (max - min) + min);
};

export const randomNumberOfOffers = getRandomCeil(23, 45);