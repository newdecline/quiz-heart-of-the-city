import React from "react";

import InstagramIcon from "../svg/instagram-social.svg";
import FacebookIcon from "../svg/facebook-social.svg";
import VkIcon from "../svg/vk-social.svg";
import YoutoobeIcon from "../svg/youtoobe-social.svg";

export const socialList = [
    {
        href: 'https://www.instagram.com/an_serdcegoroda/?hl=ru',
        icon: <InstagramIcon/>,
    },
    {
        href: 'https://www.facebook.com/serdce.goroda',
        icon: <FacebookIcon/>,
    },
    {
        href: 'https://vk.com/an_serdcegoroda',
        icon: <VkIcon/>,
    },
    {
        href: 'https://www.youtube.com/channel/UCMspr_XqOHT1yDclw2a1Sew',
        icon: <YoutoobeIcon/>,
    },
];