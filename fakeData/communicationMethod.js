import React from "react";

import WhatsAppIcon from "../svg/whats-app.svg";
import ViberIcon from "../svg/viber.svg";
import TelegramIcon from "../svg/telegram.svg";
import PhoneIcon from "../svg/phone.svg";

export const communicationMethod = [
    {
        id: 1,
        icon: <WhatsAppIcon/>,
        text: 'WhatsApp',
        label: 'WhatsApp'
    },
    {
        id: 2,
        icon: <ViberIcon/>,
        text: 'Viber',
        label: 'Viber'
    },
    {
        id: 3,
        icon: <TelegramIcon/>,
        text: 'Telegram',
        label: 'Telegram'
    },
    {
        id: 3,
        icon: <PhoneIcon/>,
        text: 'Phone',
        label: 'Телефон'
    }
];