const express = require('express');
const next = require('next');
const proxy = require('http-proxy-middleware');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const optionsProxy = proxy({
    target: process.env.BASE_URL,
    changeOrigin: true
});

app
    .prepare()
    .then(() => {
        const server = express();

        dev && server.use(express.static('public'));
        server.use(['/api'], optionsProxy);

        server.get('/', (req, res) => {
            const actualPage = '/index';
            app.render(req, res, actualPage);
        });

        server.get('/agreement', (req, res) => {
            const actualPage = '/agreement';
            app.render(req, res, actualPage);
        });

        server.get('*', (req, res) => {
            return handle(req, res)
        });

        const listenCallback = err => {
            if (err) throw err;
            console.log(`> Ready on http://localhost:${port}`)
        };

        dev ? server.listen(port, listenCallback) : server.listen(port, 'localhost', listenCallback);
    })
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });