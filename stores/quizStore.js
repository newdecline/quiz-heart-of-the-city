import {observable, action, computed} from 'mobx';
import {createViewModel} from "mobx-utils";

const initialQuizStore = observable({
    isOpenQuizPopup: false,
    activeQuestion: 0,
    communicationMethod: null,
    quiz: [
        {
            id: 1,
            question: 'В каких административных округах вас интересуют квартиры?',
            hint: 'Можно выбрать несколько вариантов ответов',
            canChoose: null,
            canSkip: false,
            answers: [
                {text: 'Центральный', id: 1, isSelected: false},
                {text: 'Северный', id: 2, isSelected: false},
                {text: 'Северо–Восточный', id: 3, isSelected: false},
                {text: 'Восточный', id: 4, isSelected: false},
                {text: 'Юго–Восточный', id: 5, isSelected: false},
                {text: 'Южный', id: 6, isSelected: false},
                {text: 'Юго-Западный', id: 7, isSelected: false},
                {text: 'Западный', id: 8, isSelected: false},
                {text: 'Северо-Западный', id: 9, isSelected: false},
                {text: 'Все районы', id: 10, isSelected: false}
            ],
            isReachGoal: false
        },
        {
            id: 2,
            question: 'Количество комнат в квартире?',
            hint: 'Можно выбрать 2 варианта ответов',
            canChoose: 2,
            canSkip: false,
            answers: [
                {text: 'Студия', id: 1, isSelected: false},
                {text: '1 комната', id: 2, isSelected: false},
                {text: '2 комнаты', id: 3, isSelected: false},
                {text: '3 комнаты', id: 4, isSelected: false},
                {text: '4 комнаты', id: 5, isSelected: false},
                {text: 'Более 4-х комнат', id: 6, isSelected: false},
            ],
            isReachGoal: false
        },
        {
            id: 3,
            question: 'Площадь квартиры?',
            hint: 'Можно выбрать 2 варианта ответов',
            canChoose: 2,
            canSkip: false,
            answers: [
                {text: '35 - 50 кв.м.', id: 1, isSelected: false},
                {text: '50 - 65 кв.м.', id: 2, isSelected: false},
                {text: '65 - 80 кв.м.', id: 3, isSelected: false},
                {text: '80 - 100 кв.м.', id: 4, isSelected: false},
                {text: 'от 100 кв.м. и больше', id: 5, isSelected: false},
            ],
            isReachGoal: false
        },
        {
            id: 4,
            question: 'Какой нужен ремонт?',
            hint: 'Можно выбрать несколько вариантов ответов',
            canChoose: 4,
            canSkip: false,
            answers: [
                {text: 'Без отделки', id: 1, isSelected: false},
                {text: 'Черновая отделка', id: 2, isSelected: false},
                {text: 'Предчистовой', id: 3, isSelected: false},
                {text: 'Чистовой', id: 4, isSelected: false},
            ],
            isReachGoal: false
        },
        {
            id: 5,
            question: 'Пожелания к внутренней инфраструктуре жилого комплекса.',
            hint: 'Можно выбрать несколько вариантов ответов',
            canChoose: 8,
            canSkip: true,
            answers: [
                {text: 'Консьерж / ресепшн', id: 1, isSelected: false},
                {text: 'Бассейн', id: 2, isSelected: false},
                {text: 'Фитнес', id: 3, isSelected: false},
                {text: 'Панорамные окна', id: 4, isSelected: false},
                {text: 'Приточно-вытяжная вентиляция', id: 5, isSelected: false},
                {text: 'Подземная парковка, многоуровневая наземная парковка', id: 6, isSelected: false},
                {text: 'Продуктовый магазин, аптека в доме', id: 7, isSelected: false},
                {text: 'Кондиционирование', id: 8, isSelected: false},
                {text: 'Закрытая территория', id: 9, isSelected: false},
            ],
            isReachGoal: false
        },
        {
            id: 6,
            question: 'Пожелания к инфраструктуре района.',
            hint: 'Можно выбрать несколько вариантов ответов или пропустить этот вопрос',
            canChoose: 7,
            canSkip: true,
            answers: [
                {text: `Близость метро (до 500 м.)`, id: 1, isSelected: false},
                {text: `Близость ТРК (до 1000 м.)`, id: 2, isSelected: false},
                {text: `Близость школы \n(до 1000 м.)`, id: 3, isSelected: false},
                {text: `Близость детского сада \n(до 500 м.)`, id: 4, isSelected: false},
                {text: `Близость остановки общественного танспорта (до 500 м.)`, id: 5, isSelected: false},
                {text: `Близость медицинского учреждения (до 1 000 м.)`, id: 6, isSelected: false},
                {text: `Близость парковой зоны (до 1000 м.)`, id: 7, isSelected: false},
            ],
            isReachGoal: false
        },
        {
            id: 7,
            question: 'Стоимость квартиры, руб.',
            hint: 'Можно выбрать несколько вариантов ответов или пропустить этот вопрос',
            canChoose: 2,
            canSkip: false,
            answers: [
                {text: '10 000 000 - 15 000 000', id: 1, isSelected: false},
                {text: '15 000 000 - 25 000 000', id: 2, isSelected: false},
                {text: '25 000 000 - 50 000 000', id: 3, isSelected: false},
                {text: '50 000 000 - 75 000 000', id: 4, isSelected: false},
                {text: 'от 75 000 000 и больше', id: 5, isSelected: false},
            ],
            isReachGoal: false
        },
    ],
    get numberOfSelectedAnswersInQuestion() {
        return this.quiz[this.activeQuestion].answers.filter(answer => answer.isSelected);
    },
    setAnswer(answerId) {
        const isSelected = this.quiz[this.activeQuestion].answers[answerId - 1].isSelected;

        this.quiz[this.activeQuestion].answers[answerId - 1].isSelected = !isSelected;
    },
    setCommunicationMethod(method) {
        this.communicationMethod = method;
    },
    setInitialStore() {
        this.reset();
        this.quiz.forEach(item => {
            item.answers.forEach(item => {
                item.isSelected = false
            })
        })
    },
    setReachGoal(prevQuestion) {
        this.quiz[prevQuestion].isReachGoal = true;
    },
    setAnswersSkip(prevQuestion) {
        this.quiz[prevQuestion].answers.forEach(item => {
            item.isSelected = false
        });
    }
}, {
    setAnswer: action,
    setInitialStore: action,
    setCommunicationMethod: action,
    setReachGoal: action,
    numberOfSelectedAnswersInQuestion: computed
});

export const quizStore = createViewModel(initialQuizStore);
