import { useStaticRendering } from 'mobx-react';
import {quizStore} from './quizStore';

const isServer = typeof window === 'undefined';
useStaticRendering(isServer);

let store = null;

export default function initializeStore() {
    if (isServer) {
        return {
            quizStore
        };
    }
    if (store === null) {
        store = {
            quizStore
        };
    }

    return store;
}