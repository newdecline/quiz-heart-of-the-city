import styled from "styled-components";

export const StyledQuizPopup = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 2;
    overflow: auto;
    .wrapper {
        margin-bottom: 30px;
        position: relative;
        display: flex;
        flex-direction: column;
        top: 75px;
        left: 50%;
        transform: translate(-50%, 0);
        width: calc(100% - 48px);
        background: #fff;
        box-shadow: 0 4px 30px rgba(0, 0, 0, 0.3);
        border-radius: 1px;
    }
    .close-quiz-popup {
        position: absolute;
        top: 20px;
        right: 12px;
        border: none;
        padding: 0;
        margin: 0;
        background-color: transparent;
    }
    @media (min-width: 1024px) {
        position: absolute;
        .wrapper {
            position: absolute;
            width: 652px;
            height: 541px;
            transform: unset;
            left: calc(72.2% - 652px);
        }
        .close-quiz-popup {
            top: 30px;
            right: 24px;
            &:hover {
                cursor: pointer;
            }
        }
    }
    @media (min-width: 1920px) {
        .wrapper {
            left: calc(55.1% - 652px);
            top: 236px;
        }
    }
`;