import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import {ProgressBar} from "../ProgressBar/ProgressBar";
import CrossIcon from "../../svg/cross.svg";
import {inject, observer} from "mobx-react";
import {Question} from "../Question/Question";
import {CommunicationMethod} from "../CommunicationMethod/CommunicationMethod";
import {StyledQuizPopup} from "./styled";


export const QuizPopup = inject('quizStore')(observer(props => {
    const {
        className,
        quizStore,
        quizStore: {
            activeQuestion,
            quiz
        }
    } = props;

    const [isEndAnswers, setIsEndAnswers] = useState(false);
    useEffect(() => {
        setIsEndAnswers(activeQuestion === quiz.length - 1);
    }, [activeQuestion]);

    const [isShowCommunicationMethod, setIsShowCommunicationMethod] = useState(false);
    useEffect(() => {
        if (isEndAnswers && isShowCommunicationMethod) {
            setIsShowCommunicationMethod(false);
        }
    }, [activeQuestion, isEndAnswers]);

    const closeQuizPopup = () => {
        quizStore.setInitialStore();
    };

    const nextAnswer = () => {
        if (!isEndAnswers) {
            quizStore.activeQuestion += 1;
        } else {
            setIsShowCommunicationMethod(true);
        }

        if (!quiz[activeQuestion].isReachGoal) {
            quizStore.setReachGoal(activeQuestion);
            ym(process.env.YMID, 'reachGoal', `answerQuestion${activeQuestion + 1}`);
        //    ответ на вопрос - цель достигнута
        }
    };

    const prevAnswer = () => {
        if (isShowCommunicationMethod) {
            setIsShowCommunicationMethod(false);
        } else if (activeQuestion !== 0) {
            quizStore.activeQuestion -= 1;
        }
    };

    return (
        <StyledQuizPopup className={className}>
            <div className="wrapper">
                <ProgressBar
                    activeQuestion={activeQuestion}
                    quiz={quiz}/>
                <button
                    onClick={closeQuizPopup}
                    className="close-quiz-popup"><CrossIcon/></button>
                <CommunicationMethod
                    isShow={isShowCommunicationMethod}
                    prevAnswer={prevAnswer}/>
                <Question
                    isShow={!isShowCommunicationMethod}
                    nextAnswer={nextAnswer}
                    prevAnswer={prevAnswer}/>
            </div>
        </StyledQuizPopup>
    )
}));

