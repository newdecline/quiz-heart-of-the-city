import styled from 'styled-components';

export const StyledCommunicationMethod = styled('div')`
    padding-bottom: 17px;
    display: ${({isShow}) => isShow ? 'flex' : 'none'};
    flex: 1;
    flex-direction: column;
    box-sizing: border-box;
    .question {
        &-header {
            padding: 29px 15px 17px 21px;
        }
        &-title {
            margin-bottom: 3px;
            color: #dfa656;
            font-size: 14px;
            line-height: 16px;
        }
        &-subtitle {
            margin-bottom: 23px;
            color: #dfa656;
            font-size: 14px;
            line-height: 16px;
        }
        &-hint {
            font-size: 14px;
            line-height: 16px;
            color: #333;
            white-space: pre-wrap;
        }
    }
    .list-communication {
        height: 260px;
        padding: 0 15px 0 21px;
        margin: 0 0 auto 0;
        list-style-type: none;
        box-sizing: border-box;
        overflow: auto;
        &__item {
            margin-bottom: 34px;
            padding-left: 36px;
            display: flex;
            align-items: center;
            box-sizing: border-box;
            .icon-wrap {
                width: 30px;
                margin-right: 18px;
            }
            .text {
                position: relative;
                &::before {
                    content: '';
                    position: absolute;
                    top: 2px;
                    left: -83px;
                    width: 14px;
                    height: 14px;
                    border-radius: 50%;
                    border: 2px solid #C4C4C4;
                    box-sizing: border-box;
                    transition: background-color .3s;
                }
            }
            &.selected {
                .text::before {
                    background-color: #c4c4c4;
                }
            }
            &:last-child {
                margin-bottom: 0;
            }
            
        }
    }
    .question-footer-buttons {
        padding: 0 16px 0 21px;
        margin: auto 0 0 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .next-step {
        padding: 6px 28px;
        font-size: 14px;
        line-height: 17px;
        letter-spacing: 0.1em;
        font-family: 'Gilroy-ExtraBold';
        text-transform: uppercase;
        color: #fff;
        background: #DFA656;
        border-radius: 1px;
        box-sizing: border-box;
        border: 1px solid transparent;
        &:disabled {
            background: transparent;
            border: 1px solid #DFA656;
            color: #DFA656;
        }
    }
    .back-step {
        display: flex;
        flex-wrap: wrap;
        align-items: baseline;
        padding: 0;
        font-size: 12px;
        line-height: 14px;
        letter-spacing: 0.1em;
        text-transform: uppercase;
        background: transparent;
        border-radius: 1px;
        box-sizing: border-box;
        border: none;
        color: #c4c4c4;
        svg {
            margin-right: 5px;
        }
    }
    .back-step__skip {
        margin-left: 27px;
    }
    .indicate-phone-number {
        display: flex;
        flex-direction: column;
        flex: 1;
        .question-footer-buttons {
            margin-top: auto;
        }
    }
    @media (min-width: 1024px) {
        padding-bottom: 43px;
        .question {
            &-header {
              padding: 46px 15px 32px 73px;
            }
            &-title {
                margin-bottom: 11px;
                font-size: 24px;
                line-height: 28px;
            }
            &-subtitle {
                margin-bottom: 50px;
                font-size: 24px;
                line-height: 28px;
            }
            &-hint {
                font-size: 18px;
                line-height: 21px;
            }
        }
        .list-communication {
            height: auto;
            display: flex;
            padding: 0 15px 0 73px;
            &__item {
                align-items: center;
                flex-direction: column;
                padding-left: 24px;
                margin-right: 57px;
                &:last-child {
                    margin-right: 0;
                }
                .icon-wrap {
                    margin-right: 0;
                    margin-bottom: 9px;
                    height: 30px;
                }
                .text::before {
                    left: -23px;
                }
                &:hover {
                    cursor: pointer;
                }
            }
        }
        .question-footer-buttons {
            padding: 0 48px 0 73px;
            margin: 0;
        }
        .back-step {
            font-size: 18px;
            line-height: 21px;
            &__text {
                display: flex;
                align-items: center;
            }
            &:hover {
                cursor: pointer;
            }
        }
        .next-step {
            padding: 11px 81px;
            font-size: 24px;
            line-height: 29px;
            &:hover {
                cursor: pointer;
            }
        }
    }
`;