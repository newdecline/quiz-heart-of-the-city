import React, {useState} from 'react';
import uuid from "uuid/v4";
import {communicationMethod} from '../../fakeData/communicationMethod';
import ArrowBackIcon from "../../svg/arrow-back.svg";
import {inject, observer} from "mobx-react";
import {quizStore} from "../../stores/quizStore";
import {IndicatePhoneNumber} from "../IndicatePhoneNumber/IndicatePhoneNumber";
import {randomNumberOfOffers} from "../../fakeData/randomNumberOfOffers";
import {StyledCommunicationMethod} from "./styled";

export const CommunicationMethod = inject('quizStore')(observer(props => {
    const {prevAnswer, isShow, quizStore} = props;

    const [isShowIndicatePhoneNumber, setIsShowIndicatePhoneNumber] = useState(false);

    const handleClickCommunicationMethod = (method) => {
        quizStore.setCommunicationMethod(method);
    };

    const handleClickNextBtn = () => {
        setIsShowIndicatePhoneNumber(true);
        ym(process.env.YMID, 'reachGoal', `feedbackType`);
    //    указан способ связи - цель достигнута
    };

    return (
        <StyledCommunicationMethod isShow={isShow}>
            {isShowIndicatePhoneNumber
                ? <IndicatePhoneNumber setIsShowIndicatePhoneNumber={setIsShowIndicatePhoneNumber}/>
                : <>
                    <div className="question-header">
                        <div className="question-title">Спасибо за уделенное время.</div>
                        <div className="question-subtitle">Мы уже подобрали для вас {randomNumberOfOffers} подходящих варианта.</div>
                        <div className="question-hint">Укажите удобный для вас способ&nbsp;связи.</div>
                    </div>
                    <ul className="list-communication">
                        {
                            communicationMethod.map(item => {
                                return (
                                    <div key={uuid()}
                                         className={quizStore.communicationMethod === item.text
                                             ? 'list-communication__item selected'
                                             : 'list-communication__item'}
                                         onClick={() => handleClickCommunicationMethod(item.text)}>
                                        <span className="icon-wrap">{item.icon}</span>
                                        <span className="text">{item.label}</span>
                                    </div>
                                )
                            })
                        }
                    </ul>
                    <div className="question-footer-buttons">
                        <button
                            onClick={prevAnswer}
                            className="back-step"
                        ><ArrowBackIcon/>Назад
                        </button>
                        <button
                            disabled={quizStore.communicationMethod === null}
                            onClick={handleClickNextBtn}
                            className="next-step">Далее
                        </button>
                    </div>
                </>}
        </StyledCommunicationMethod>
    )
}));

