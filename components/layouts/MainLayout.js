import React from "react";
import {Header} from "../Header/Header";
import {Footer} from "../Footer/Footer";
import Head from "next/head";

const isProduction = process.env.NODE_ENV === 'production';

const ymJs = `
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(${process.env.YMID}, "init", {
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
});
`;

const pixelJs = `
!function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1245927438937063');
  fbq('track', 'PageView');
`;

const vkJs = `
!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?161",t.onload=function(){VK.Retargeting.Init("VK-RTRG-388105-grRWa"),VK.Retargeting.Hit()},document.head.appendChild(t)}();
`;

const consultant = `
(function(w,d,u){
var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
})(window,document,'https://serdce-goroda24.ru/upload/crm/site_button/loader_4_h33w19.js');
`;

export const MainLayout = props => {
  const {
    children,
  } = props;

  return (
    <>
      <Head>
        <title>Купи квартиру в Москве</title>
        <meta name="description"
              content="Все новостройки Москвы в одном месте. Пройди тест за 3 минуты и получи бесплатную подборку квартир по индивидуальным пожеланиям."/>
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width"/>
        <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.ico"/>
        {(isProduction) && <>
          <script dangerouslySetInnerHTML={{__html: ymJs}}/>
          <noscript>
            <div>
              <img src={`https://mc.yandex.ru/watch/${process.env.YMID}`}
                   style={{position: 'absolute', left: -9999}} alt=""/>
            </div>
          </noscript>

          <script dangerouslySetInnerHTML={{__html: pixelJs}}/>
          <noscript><img height="1" width="1" style={{display: 'none'}}
                         src="https://www.facebook.com/tr?id=1245927438937063&ev=PageView&noscript=1"
          /></noscript>

          <script type="text/javascript" dangerouslySetInnerHTML={{__html: vkJs}}/>
          <noscript><img src="https://vk.com/rtrg?p=VK-RTRG-388105-grRWa" style={{position: 'fixed', left: -999}}
                         alt=""/></noscript>

          <script dangerouslySetInnerHTML={{__html: consultant}}/>
        </>}
      </Head>
      <Header/>
      {children}
      <Footer/>
    </>
  )
};