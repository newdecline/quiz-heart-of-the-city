import styled from "styled-components";

export const StyledFirstSection = styled('div')`
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 3.4fr 1.6fr;
    grid-template-areas: 'content-block' 'picture-block';
    box-sizing: border-box;
    height: 100vh;
    .start-quiz {
        display: ${({isOpenQuizPopup}) => isOpenQuizPopup ? 'none' : 'block'};
        padding: 12px 17px;
        border: none;
        position: absolute;
        bottom: -52px;
        left: 50%;
        transform: translate(-50%, -50%);
        background: #DFA656;
        border-radius: 1px;
        color: #fff;
        font-size: 24px;
        line-height: 29px;
        letter-spacing: 0.1em;
        text-transform: uppercase;
        font-family: 'Gilroy-ExtraBold';
        white-space: nowrap;
        z-index: 1;
    }
    .title {
        display: ${({isOpenQuizPopup}) => isOpenQuizPopup ? 'none' : 'block'};
        margin: 25px 0 20px 0;
        font-size: 30px;
        line-height: 36px;
        font-family: 'Gilroy-ExtraBold';
        color: #DFA656;
        z-index: 1;
    }
    .subtitle {
        display: ${({isOpenQuizPopup}) => isOpenQuizPopup ? 'none' : 'block'};
        font-size: 16px;
        line-height: 19px;
        font-weight: 400;
        z-index: 1;
    }
    .content-block {
        position: relative;
        padding: 0 34px;
        grid-area: content-block;
        background-image: linear-gradient(to right, rgba(0,0,0,.5), rgba(0,0,0,.5)), url('assets/images/backgroung-texture.jpg');
        background-size: cover;
        box-sizing: border-box;
        color: #fff;
        &__text {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translateX(-50%);
            width: calc(100% - 68px);
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }
        .more {
            display: none;
        }
    }
    .picture-block {
        position: relative;
        grid-area: picture-block;
        background-image: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%), url('assets/images/background-index-mob.jpg');
        background-size: cover;
        .more {
            bottom: 29px;
        }
    }
    .more {
        display: ${({isOpenQuizPopup}) => isOpenQuizPopup ? 'none' : 'flex'};
        flex-direction: column;
        align-items: center;
        position: absolute;
        bottom: -120px;
        left: 50%;
        transform: translate(-50%, -50%);
        border: none;
        padding: 0;
        margin: 0;
        background-color: transparent;
        &__text {
            margin-bottom: 5px;
            color: #DFA656;
            font-size: 16px;
            line-height: 19px;
        }
    }
    .social-list {
        display: none;
    }
    @media (min-width: 1024px) {
        grid-template-columns: 72.2% 27.8%;
        grid-template-rows: auto;
        grid-template-areas: 'content-block picture-block';
        .content-block {
            padding: 122px 34px 0 90px;
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            &__text {
                height: auto;
                flex: 1 0 auto;
                position: unset;
                max-width: 471px;
                transform: unset;
            }
        }
        .title {
            margin: 0 0 23px 0;
            font-size: 42px;
            line-height: 120%;
        }
        .subtitle {
            margin: 0;
            font-size: 24px;
            line-height: 120%;
        }
        .start-quiz {
            margin-bottom: 39px;
            position: relative;
            top: unset;
            bottom: unset;
            left: unset;
            transform: none;
            transition: background-color .3s;
            &:hover {
                cursor: pointer;
                background: #C7934C;
            }
        }
        .picture-block {
            background-image: url('assets/images/background-index-desktop.jpg');
            background-position: 0 -80px;
            background-size: unset;
            background-repeat: no-repeat;
            .more {
                display: none;
            }
        }
        .content-block {
            .more {
                position: relative;
                padding-bottom: 35px;
                display: ${({isOpenQuizPopup}) => isOpenQuizPopup ? 'none' : 'flex'};
                flex-direction: row;
                bottom: unset;
                left: unset;
                transform: unset;
                &:hover {
                    cursor: pointer;
                }
                &__text {
                    font-size: 18px;
                    line-height: 21px;
                    margin-bottom: 0;
                    margin-right: 12px;
                }
            }   
        }
        .social-list {
            position: absolute;
            right: 0;
            top: 0;
            display: flex;
            flex-direction: column;
            list-style-type: none;
            padding: 0;
            margin: 0 0 0 0;
            &__item {
                padding: 14px;
                box-sizing: border-box;
                background: #DFA656;
                border-bottom: 1px solid #C7934C;
                transition: background .3s;
                &:last-child {
                    border-bottom: none;
                }
                &:hover {
                    cursor: pointer;
                    background: #C7934C;
                }
            }
            a {
                display: flex;
            svg {
                path, ellipse {
                    fill: #fff;
                }
                rect, circle {
                    stroke: #fff;
                }
            }
        }
    }
    @media (min-width: 1920px) {
        grid-template-columns: 55.1% 44.9%;
        position: relative;
        .picture-block {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: unset;
        }
        .content-block {
            padding: 0 0 0 360px;
            &__text {
                max-width: 624px;
            }
        }
        .start-quiz {
            margin-bottom: 100px;
            padding: 12px 24px;
        }
        .title {
            max-width: 632px;
            margin: 160px 0 50px 0;
            font-size: 54px;
            line-height: 66px;
        }
        .subtitle {
            font-size: 36px;
            line-height: 130%;
        }
    }
}`;
