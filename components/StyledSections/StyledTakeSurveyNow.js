import styled from "styled-components";

export const StyledTakeSurveyNow = styled('div')`
    .start-quiz {
        padding: 12px 17px;
        border: none;
        position: absolute;
        bottom: -49px;
        left: 50%;
        transform: translate(-50%, -50%);
        background: #DFA656;
        border-radius: 1px;
        color: #fff;
        font-size: 24px;
        line-height: 29px;
        letter-spacing: 0.1em;
        text-transform: uppercase;
        white-space: nowrap;
        font-family: 'Gilroy-ExtraBold', sans-serif;
        z-index: 1;
    }
    .content {
        position: relative;
        padding: 40px 34px 65px 34px;
        background-image: linear-gradient(to right, rgba(0,0,0,.5), rgba(0,0,0,.5)), url(assets/images/backgroung-texture.jpg);
        background-size: cover;
    }
    .title {
        margin: 0 0 9px 0;
        font-size: 30px;
        line-height: 36px;
        font-family: 'Gilroy-ExtraBold', sans-serif;
        color: #DFA656;
    }
    .subtitle { 
        margin: 0;
        font-size: 16px;
        line-height: 19px;
        color: #fff;
    }
    .picture {
        min-height: 181px;
        background-image: url('assets/images/gyrl-mob.jpg');
        background-size: cover;
    }
    @media (min-width: 1024px) {
        display: flex;
        .title {
            margin: 0 0 52px 0;
            max-width: 611px;
            font-size: 54px;
            line-height: 66px;
        }
        .subtitle {
            max-width: 569px;
            margin-bottom: 51px;
            font-size: 30px;
            line-height: 35px;
        }
        .content {
            width: 72.26%;
            padding: 52px 0 67px 90px;
            box-sizing: border-box;
        }
        .picture {
            width: 28.74%;
            min-height: auto;
            background-image: url('assets/images/gyrl-1024.jpg');
        }
        .start-quiz {
            padding: 12px 30px;
            position: unset;
            transform: unset;
            transition: background-color .3s;
            &:hover {
                cursor: pointer;
                background: #C7934C;
            }
        }
    }
    @media (min-width: 1920px) {
        .content {
            padding: 22px 0 73px 360px; 
            width: 55.1%;
        }
        .title {
            margin: 0 0 40px 0;
            max-width: 631px;
        }
        .subtitle {
            margin-bottom: 62px;
            font-size: 36px;
            line-height: 42px;
        }
        .picture {
            width: 44.9%;
            background-image: url('assets/images/gyrl-1920.jpg');
        }
    }
`;