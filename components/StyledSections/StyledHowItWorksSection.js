import styled from "styled-components";

export const StyledHowItWorksSection = styled('div')`
    padding: 3px 26px 47px 34px;
    background: #000;
    color: #fff;
    box-sizing: border-box;
    .title {
        padding-right: 15px;
        margin: 0 0 17px 0;
        font-size: 30px;
        line-height: 36px;
        color: #DFA656;
        font-family: 'Gilroy-ExtraBold';
    }
    .subtitle {
        margin: 0 0 21px 0;
        font-size: 16px;
        line-height: 19px;
        font-weight: 400;  
    }
    .steps {
        &__item-wrap {
            margin-bottom: 29px;
            &:last-child {
                margin-bottom: 0;
            }
        }
        &__item {
            position: relative;
            display: flex;
            flex-direction: column;
        }
        .item {
            &-title {
                margin-bottom: 7px;
                font-size: 16px;
                line-height: 20px;
                font-family: 'Gilroy-ExtraBold';
            }
            &-subtitle {
                font-size: 16px;
                line-height: 19px;
                font-weight: 400;
            }
            &-number {
                margin-bottom: 17px;
                display: flex;
                align-items: center;
                justify-content: center;
                width: 55px;
                height: 55px;
                font-size: 25px;
                line-height: 31px;
                font-family: 'Gilroy-ExtraBold';
                background-size: cover;
                &-1 {
                    background-image: url('assets/svg/radial-progress-25.svg');
                }
                &-2 {
                    background-image: url('assets/svg/radial-progress-50.svg');
                }
                &-3 {
                    background-image: url('assets/svg/radial-progress-75.svg');
                }
                &-4 {
                    background-image: url('assets/svg/radial-progress-100.svg');
                }
            }
        }
    }
    .dots-container {
        display: none;
    }
    @media (min-width: 1024px) {
        padding: 39px 48px 49px 90px;
        .title {
            margin: 0 0 13px 0;
            font-size: 54px;
            line-height: 66px;
        }
        .subtitle {
            margin: 0 0 61px 0;
            font-size: 30px;
            line-height: 35px;
        }
        .steps {
            display: flex;
            flex-direction: row;
            &__item-wrap {
                position: relative;
                margin-bottom: 0;
                flex: 1;
                &:last-child {
                    .dots-container {
                        display: none;
                    }
                }
            }
            &__item {
                max-width: 180px;
            }
            .item {
                &-number {
                    margin-bottom: 36px;
                    width: 79px;
                    height: 79px;
                    font-size: 36px;
                    line-height: 44px;
                }
                &-title {
                    margin-bottom: 10px;
                    font-size: 18px;
                    line-height: 22px;
                }
                &-subtitle {
                    font-size: 18px;
                    line-height: 22px;
                }
            }
        }
        .dots-container {
            position: absolute;
            right: 0;
            top: 36px;
            width: calc(100% - 79px);
            display: flex;
            justify-content: center;
            box-sizing: border-box;
        }
        .dot {
            margin-left: 17px;
            &:first-child {
                margin-left: 0;
            }
            &:nth-child(5), &:nth-child(6), &:nth-child(7), &:nth-child(8) {
                display: none;
            }
        }
    }
    @media (min-width: 1920px) {
        padding: 64px 330px 80px 360px;
        .title {
            margin-bottom: 44px;
        }
        .subtitle {
            font-size: 36px;
            line-height: 42px;
            margin-bottom: 68px;
        }
        .steps {
            &__item {
                max-width: 209px;
                &-wrap {
                    width: 24.4%;
                    flex: unset;
                }
            }
            .item-number {
                margin-bottom: 45px;
            }
            .item-title {
                margin-bottom: 8px;
            }
            .item-subtitle {
                line-height: 23px;
            }
        }
        .dot {
            &:nth-child(5), &:nth-child(6), &:nth-child(7), &:nth-child(8) {
                display: block;
            }
        }
    }
`;