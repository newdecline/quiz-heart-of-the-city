import styled from "styled-components";

export const StyledOffersSection = styled('div')`
    padding: 35px 38px 54px 34px;
    background: #000;
    color: #fff;
    .you-will-get {
        margin-bottom: 30px;
    }
    .title {
        margin-bottom: 17px;
        font-family: 'Gilroy-ExtraBold';
        color: #DFA656;
        font-size: 16px;
        line-height: 20px;
    }
    .list {
        padding: 0;
        margin: 0;
        list-style-type: none;
        &__item {
            position: relative;
            margin-bottom: 14px;
            padding-left: 23px;
            font-size: 16px;
            line-height: 130%;
            &:last-child {
                margin-bottom: 0;
            }
            &::before {
                content: '';
                position: absolute;
                top: 7px;
                left: 0;
                display: block;
                width: 6px;
                height: 6px;
                border: 1px solid #fff;
                box-sizing: border-box;
                border-radius: 50%;
            }
        }
    }
    @media (min-width: 1024px) {
        padding: 38px 0 55px 90px;
        .title {
            margin-bottom: 16px;
            font-family: 'Gilroy-Regular';
            font-size: 36px;
            line-height: 42px;
        }
        .list {
            max-width: 644px;
            &__item {
                margin-bottom: 9px;
                font-size: 21px;
                line-height: 1.57;
                &::before {
                    top: 13px;
                }
            }
        }
    }
    @media (min-width: 1920px) {
        display: flex;
        padding: 43px 330px 39px 360px;
        .title {
            margin-bottom: 21px;
        }
        .list__item {
            margin-bottom: 15px;
            line-height: 1.37;
            &::before {
                top: 11px;
            }
        }
        .you-will-get {
            margin-bottom: 0;
            width: 50.5%;
        }
        .why-free {
            
        }
    }
`;