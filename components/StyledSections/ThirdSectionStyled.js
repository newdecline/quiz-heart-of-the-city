import styled from "styled-components";

export const StyledThirdSection = styled('div')`
    display: flex;
    flex-direction: column;
    .content {
        margin: 0;
        padding: 40px 34px 35px 34px;
        background-image: linear-gradient(to right,rgba(0,0,0,.5),rgba(0,0,0,.5)), url('assets/images/thrid-section.jpg');
        background-size: cover;
    }
    .text {
        margin: 0;
        position: relative;
        font-size: 16px;
        line-height: 19px;
        z-index: 1;
        color: #fff;
    }
    .picture {
        height: 180px;
        background-image: url('assets/images/city.jpg');
        background-size: cover;
        background-position: top right;
    }
    @media (min-width: 1024px) {
        flex-direction: row;
        .content {
            width: 72.26%;
            padding: 46px 0 46px 90px;
            box-sizing: border-box;
        }
        .picture {
            width: 28.74%;
            height: unset;
            background-image: url(assets/images/city-1024.jpg);
        }
        .text {
            margin: 0;
            max-width: 569px;
            font-size: 30px;
            line-height: 35px;
        }
    }
    @media (min-width: 1920px) {
        .content {
            padding: 94px 0 80px 360px;
            width: 55.1%;
        }
        .text {
            max-width: 646px;
            font-size: 36px;
            line-height: 42px;
        }
        .picture {
            width: 44.9%;
            background-image: url(assets/images/city-1920.jpg);
        }
    }
`;