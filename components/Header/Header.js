import React, {useState, useEffect, useRef} from 'react';
import styled from "styled-components";
import LogoIcon from "../../svg/logo.svg";
import PhoneIcon from "../../svg/phone.svg";
import {inject, observer} from "mobx-react";
import scrollIntoView from 'scroll-into-view';
import {disablePageScroll, enablePageScroll, getPageScrollBarWidth } from 'scroll-lock';
import {socialList} from "../../fakeData/socialList";
import {StyledHeader} from "./styled";

export const Header = inject('quizStore')(observer(props => {
    const {quizStore} = props;

    const menuRef = useRef(null);

    const [isOpenMenu, setIsOpenMenu] = useState(false);
    useEffect(() => {
        if (process.browser) {
            const $consultantWrapper = document.querySelector('.b24-widget-button-position-bottom-left');

            if (isOpenMenu) {
                disablePageScroll();
                menuRef.current.style.left = `-${getPageScrollBarWidth() / 2}px`;
                $consultantWrapper && $consultantWrapper.classList.add('hidden-consultant-when-open-menu');
            } else {
                enablePageScroll();
                $consultantWrapper && $consultantWrapper.classList.remove('hidden-consultant-when-open-menu');
            }
        }
    }, [isOpenMenu]);

    const handleClickBurgerBtn = () => {
        setIsOpenMenu(!isOpenMenu);
    };

    const handleClickStartQuizBtn = () => {
        quizStore.isOpenQuizPopup = true;
        handleClickBurgerBtn();
    };

    const handleClickMenuItem = (sectionId) => {
        const targetScroll = document.getElementById(sectionId);
        scrollIntoView(targetScroll, {
            time: 500,
            align:{
                top: 0,
                topOffset: 30
            }
        });
        setIsOpenMenu(false);
    };

    return (
        <StyledHeader isOpenMenu={isOpenMenu} >
            <button onClick={handleClickBurgerBtn} className="burger-menu">
                <div className='burger-menu__item'> </div>
                <div className='burger-menu__item'> </div>
                <div className='burger-menu__item'> </div>
            </button>
            <a className='logo' target='_blank' href="https://xn--80afcbcb8danee7d.xn--p1ai"><LogoIcon/></a>
            <a href='tel:88005506626' className="tel">
                <PhoneIcon/>
                <span className='tel__number'>8 800 550 66 26</span>
            </a>
            <nav className="menu" ref={menuRef}>
                <div className="menu__item" onClick={() => handleClickMenuItem('main')}>Главная</div>
                <div className="menu__item" onClick={() => handleClickMenuItem('how-it-works')}>Как это работает?</div>
                <div className="menu__item" onClick={() => handleClickMenuItem('you-will-get')}>Что вы получите?</div>
                <div className="menu__item" onClick={() => handleClickMenuItem('why-free')}>Почему это бесплатно?</div>
                <div onClick={handleClickStartQuizBtn} className="menu__item">Пройти опрос</div>
                <ul className="social-list">
                    {
                        socialList.map(item => (
                            <li key={item.href} className="social-list__item">
                                <a rel='noreferrer noopener' target='_blank' href={item.href}>{item.icon}</a>
                            </li>
                        ))
                    }
                </ul>
            </nav>
        </StyledHeader>
    )
}));