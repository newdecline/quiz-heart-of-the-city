import styled from "styled-components";

export const StyledHeader = styled('header')`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    display: flex;
    align-items: center;
    padding: 27px 26px 27px 34px;
    box-sizing: border-box;
    z-index: 2;
    .burger-menu {
        position: relative;
        width: 31px;
        height: 26px;
        background-color: transparent;
        padding: 0;
        border: none;
        order: 3;
        margin-left: 33px;
        margin-bottom: 5px;
        z-index: 1;
        &__item {
            position: absolute;
            display: block;
            height: 4px;
            background-color: #DFA656;
            transform-origin: center;
            transition: all .3s;
            &:nth-child(1) {
                top: 0;
                width: ${({isOpenMenu}) => isOpenMenu ? '100%' : '100%'};
                transform: ${({isOpenMenu}) => isOpenMenu ? 'translate3d(-2px,11px,0) rotate(45deg)' : 'translate3d(0, 0, 0)'};
            }
            &:nth-child(2) {
                top: 11px;
                width: ${({isOpenMenu}) => isOpenMenu ? '0' : '100%'};
            }
            &:nth-child(3) {
                bottom: 0;
                width: ${({isOpenMenu}) => isOpenMenu ? '100%' : '65%'};
                transform: ${({isOpenMenu}) => isOpenMenu ? 'translate3d(-2px, -11px, 0) rotate(-45deg)' : 'translate3d(0, 0, 0)'};
            }
        }
    }
    .logo {
        order: 1;
        z-index: 1;
    }
    .tel {
        order: 2;
        margin-left: auto;
        z-index: 1;
        &__number {
            display: none;
        }
    }
    .menu {
        padding: 106px 34px 0 34px;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;
        background-color: #000;
        color: #fff;
        box-sizing: border-box;
        transition: transform .3s;
        transform: ${({isOpenMenu}) => isOpenMenu ? 'translateX(0)' : 'translateX(100%)'};
        &__item {
            position: relative;
            margin-bottom: 25px;
            padding-bottom: 4px;
            display: inline-block;
            font-size: 21px;
            line-height: 130%;
            &::before {
                position: absolute;
                content: '';
                bottom: 0;
                left: 0;
                width: 0;
                height: 4px;
                background-color: #DFA656;
                transition: width .3s;
            }
        }
    }
    .social-list {
        display: flex;
        list-style-type: none;
        padding: 0;
        margin: auto 0 28px 0;
        &__item {
            margin-right: 28px;
            &:last-child {
                margin-right: 0;
            }
        }
        svg {
            path, ellipse {
                fill: #fff;
            }
            rect, circle {
                stroke: #fff;
            }
        }
    }
    @media (min-width: 1024px) {
        padding: 48px 48px 48px 90px;
        width: 72.2%;
        .burger-menu {
            width: 47px;
            order: 1;
            margin-left: 0;
            margin-right: 60px;
            &__item {
                &:nth-child(1) {
                    width: ${({isOpenMenu}) => isOpenMenu ? '70%' : '100%'};
                }
                &:nth-child(3) {
                    width: ${({isOpenMenu}) => isOpenMenu ? '70%' : '65%'};
                }
            }
            &:hover {
                cursor: pointer;
            }
        }
        .logo {
            order: 2;
            svg {
                width: 180px;
                height:auto;
            }
        }
        .tel {
            display: flex;
            align-items: center;
            order: 3;
            text-decoration: none;
            color: #fff;
            font-size: 26px;
            line-height: 30px;
            &__number {
                margin-left: 15px;
                display: inline-block;
            }
        }
        .menu {
            padding: 153px 34px 0 195px;
            width: 72.2%;
            transform: ${({isOpenMenu}) => isOpenMenu ? 'translateX(0)' : 'translateX(-100%)'};
            &__item {
                &:hover {
                    cursor: pointer;
                    &::before {
                        width: 100%;
                    }
                }
            }
        }
        .social-list {
            display: none;
        }
    }
    @media (min-width: 1920px) {
        width: 55.1%;
        padding: 50px 69px 45px 45px;
        .burger-menu {
            margin-right: 83px;
        }
        .menu {
            padding: 153px 34px 0 174px;
            width: 55.1%;
            transform: ${({isOpenMenu}) => isOpenMenu ? 'translateX(0)' : 'translateX(-100%)'};
        }
    }
`;