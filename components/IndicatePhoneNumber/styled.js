import styled from "styled-components";

export const StyledIndicatePhoneNumber = styled('div')`
    display: flex;
    flex-direction: column;
    align-items: ${({isFormSubmitted}) => isFormSubmitted ? 'center' : 'unset'};
    justify-content: ${({isFormSubmitted}) => isFormSubmitted ? 'center' : 'unset'};
    flex: 1;
    .label-phone {
        display: block;
        margin-bottom: 42px;
        &__hint {
            font-size: 12px;
            line-height: 14px;
            color: #C4C4C4;
        }
    }
    .input-phone {
        padding-bottom: 7px;
        margin-bottom: 10px;
        width: 100%;
        font-size: 24px;
        line-height: 28px;
        color: #333333;
        letter-spacing: 0.1em;
        border: none;
        border-bottom: 1px solid #333;
        box-sizing: border-box;
    }
    .buttons {
        padding: 0 16px 0 21px;
        margin: auto 0 0 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
        &__back {
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            padding: 0;
            font-size: 12px;
            line-height: 14px;
            letter-spacing: 0.1em;
            text-transform: uppercase;
            background: transparent;
            border-radius: 1px;
            box-sizing: border-box;
            border: none;
            color: #c4c4c4;
            svg {
                margin-right: 5px;
            }
        }
        &__submit {
            padding: 6px 14px;
            font-size: 14px;
            line-height: 17px;
            letter-spacing: 0.1em;
            font-family: 'Gilroy-ExtraBold';
            text-transform: uppercase;
            color: #fff;
            background: #DFA656;
            border-radius: 1px;
            box-sizing: border-box;
            border: 1px solid transparent;
            transition: border .3s, background .3s, color .3s;
            &:disabled {
                background: transparent;
                border: 1px solid #DFA656;
                color: #DFA656;
            }
        }
    }
    .question-header {
        padding: 45px 15px 11px 21px;
        white-space: pre-wrap;
    }
    .question-body {
        padding: 29px 15px 11px 21px;
    }
    .input-agreement {
        position: absolute;
        z-index: -1;
        visibility: hidden;
        &:checked + .label-agreement__text::before {
            background-color: #DFA656;
            border: 2px solid #DFA656;
        }
    }
    .label-agreement {
        display: block;
        position: relative;
        padding-left: 36px;
        //margin-bottom: 85px;
        &__text {
            font-size: 12px;
            line-height: 14px;
            color: #333333;
            &::before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                display: block;
                width: 20px;
                height: 20px;
                border: 2px solid #C4C4C4;
                border-radius: 2px;
                box-sizing: border-box;
                transition: background-color .3s, border .3s;
            }
        }
    }
    .call-us {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 65px;
        &__text {
            font-size: 14px;
            line-height: 16px;
            color: #C7934C;
        }
        &__link {
            padding: 5px 12px;
            margin-left: 13px;
            border: 2px solid #DFA656;
            border-radius: 1px; 
            font-family: 'Gilroy-ExtraBold';
            font-size: 14px;
            line-height: 17px;
            letter-spacing: 0.1em;
            color: #DFA656;
            text-transform: uppercase;
            text-decoration: none;
            box-sizing: border-box;
        }
        svg {
            display: none;
        }
    }
    .message-success {
        padding: 100px 0 85px 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        max-width: 170px;
        text-align: center;
        box-sizing: border-box;
        &__title {
            margin-bottom: 12px;
            font-size: 36px;
            line-height: 42px;
            color: #C7934C;
        }
        &__subtitle {
            font-size: 14px;
            line-height: 16px;
            color: #333333;
        }
    }
    .email-input-label {
      display: flex;
      flex-direction: column;
      margin-bottom: 33px;
      &__text {
        margin-bottom: 27px;
        color: #C7934C;
        font-size: 14px;
        line-height: 16px;
      }
      &__input {
        font-size: 24px;
        line-height: 28px;
        color: #333;
        border: none;
        border-bottom: 1px solid #333;
      }
      &__hint {
        margin-top: 7px;
        color: #C4C4C4;
        font-size: 12px;
        line-height: 14px;
      }
    }

    @media (min-width: 1024px) {
        .question-header {
          padding: 45px 15px 11px 73px;
        }
        .question-body {
             padding: 29px 48px 11px 73px;
             margin-bottom: auto;
        }
        .label-phone {
            width: 260px;
        }
        .call-us {
            align-items: center;
            &__link {
                margin-left: 0;
                padding: 9px 22px;
                font-size: 24px;
                line-height: 29px;
            }
            &__text {
                font-size: 24px;
                line-height: 28px;
            }
            svg {
                display: inline-block;
                margin-right: 20px;
            }
        }
        .buttons {
            padding: 0 48px 0 73px;
            margin: 0;
            &__back {
                font-size: 18px;
                line-height: 21px;
                &:hover {
                    cursor: pointer;
                }
            }
            &__submit {
                padding: 11px 47px;
                font-size: 24px;
                line-height: 29px;
                &:hover {
                    cursor: pointer;
                }
            }
        }
        .message-success {
            max-width: unset;
            padding: 0;
            &__title {
                font-size: 64px;
                line-height: 74px;
            }
            &__subtitle {
                font-size: 24px;
                line-height: 28px;
            }
        }
        .label-agreement {
            &:hover {
                cursor: pointer;
            }
        }
        .email-input-label {
          &__text {
            max-width: 441px;
            font-size: 24px;
            line-height: 28px;
          }
          &__input {
            max-width: 223px;
          }
          &__hint {
            
          }
        }

    }
`;