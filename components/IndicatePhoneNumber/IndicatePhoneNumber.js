import React, {useState, useEffect} from 'react';
import ArrowBackIcon from "../../svg/arrow-back.svg";
import InputMask from 'react-input-mask';
import {useHttp} from '../../hooks/http.hook';
import {inject, observer} from "mobx-react";
import {quizStore} from "../../stores/quizStore";
import {StyledIndicatePhoneNumber} from "./styled";

const regExp = /^\+\d\(\d{3}\)\d{7}/;

export const IndicatePhoneNumber = inject('quizStore')(observer(props => {
  const {setIsShowIndicatePhoneNumber} = props;

  const {loading, request} = useHttp();

  const [valuePhone, setValuePhone] = useState('');
  const [valueEmail, setValueEmail] = useState('');
  const [isAgreement, setIsAgreement] = useState(false);
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);

  const [isValidPhone, setIsValidPhone] = useState(false);
  useEffect(() => {
    setIsValidPhone(!!valuePhone.replace(/\s/g, '').match(regExp));
  }, [valuePhone]);

  const onChangeInput = (e) => {
    setValuePhone(e.target.value);
  };

  const onChangeEmail = (e) => {
    setValueEmail(e.target.value);
  };

  const onChangeAgreement = (e) => {
    setIsAgreement(e.target.checked)
  };

  const handleClickCallBtn = () => {
    ym(process.env.YMID, 'reachGoal', `callButton`);
    //    нажа кнопка позвонить - цель достигнута
  };

  const onSubmit = async () => {
    const tmpRes = [];

    quizStore.quiz.forEach((item, index) => {
      tmpRes[index] = {
        question: item.question,
        answers: item.answers.filter(item => item.isSelected).map(item => item.text)
      };
    });

    const message = tmpRes.map((answer, index) => {
      return (
        `${index + 1}. "${answer.question}" \nОтвет: ${answer.answers.map(item => `"${item}"`).join(', ')}\n\n`
      )
    });

    const {status} = await request('/lead', {
      method: 'post',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        phone: valuePhone.replace(/\s/g, '').replace(/\(/g, '').replace(/\)/g, ''),
        messenger: quizStore.communicationMethod,
        description: message.join(' '),
        email: valueEmail
      })
    });

    if (status.ok) {
      setIsFormSubmitted(true);
      ym(process.env.YMID, 'reachGoal', `formSubmitted`);
      //    форма отправлена - цель достигнута
    }
  };

  return (
    <StyledIndicatePhoneNumber isFormSubmitted={isFormSubmitted}>
      {isFormSubmitted
        ? <div className="message-success">
          <div className="message-success__title">Спасибо!</div>
          <div className="message-success__subtitle">Мы свяжемся с вами в&nbsp;ближайшее&nbsp;время.</div>
        </div>
        : <>
          <div className="question-header">
            <div className="question-title">Укажите ваш номер телефона для&nbsp;связи.</div>
          </div>
          <div className="question-body">
            <label className="label-phone">
              <InputMask
                alwaysShowMask
                mask="+7 (999) 999 99 99"
                className="input-phone"
                value={valuePhone}
                onChange={onChangeInput}
                autoFocus
              />
              <div className="label-phone__hint">Ваш номер телефона*</div>
            </label>

            <label className="email-input-label">
              <span
                className="email-input-label__text">Укажите ваш e-mail, чтобы получать от нас интересные предложения</span>
              <input value={valueEmail} onChange={onChangeEmail} type="email" className="email-input-label__input"/>
              <span className="email-input-label__hint">Ваш e-mail</span>
            </label>

            <label className="label-agreement">
              <input onChange={onChangeAgreement} type="checkbox" className="input-agreement"/>
              <span className="label-agreement__text">Даю согласие на обработку персональных данных</span>
            </label>

          </div>
          <div className="buttons">
            <button
              onClick={() => setIsShowIndicatePhoneNumber(false)}
              className="buttons__back"
            ><ArrowBackIcon/>Назад
            </button>
            <button
              type='submit'
              disabled={!isValidPhone || !isAgreement || loading}
              onClick={onSubmit}
              className="buttons__submit">{loading ? "Отправляется" : "Отправить"}
            </button>
          </div>
        </>}

    </StyledIndicatePhoneNumber>
  )
}));