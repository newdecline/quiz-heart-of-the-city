import styled from "styled-components";

export const StyledProgressBar = styled('div')`
    position: relative;
    height: 7px;
    background-color: #d2d2d7;
    .fill {
        position: absolute;
        top: 0;
        left: 0;
        width: ${({valueFill}) => `${valueFill}%`};
        height: 100%;
        background-color: #dfa656;
        transition: width .3s;
    }
`;