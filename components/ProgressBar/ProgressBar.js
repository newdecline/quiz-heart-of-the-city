import React from 'react';
import {StyledProgressBar} from "./styled";

export const ProgressBar = props => {
    const {activeQuestion, quiz} = props;

    const valueFill = ((activeQuestion + 1)  / quiz.length) * 100;

    return (
        <StyledProgressBar valueFill={valueFill}>
            <div className="fill"> </div>
        </StyledProgressBar>
    )
};