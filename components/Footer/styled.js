import styled from 'styled-components';

export const StyledFooter = styled('footer')`
    display: flex;
    flex-direction: column;
    padding: 29px 24px 29px 34px;
    background: #000;
    color: #dfa656;
    .copyright, .address {
        margin-bottom: 12px;
        font-size: 16px;
        line-height: 19px;
    }
    .address {
        margin-bottom: 16px;
    }
    a {
        color: #dfa656;
        text-decoration: none;
    }
    .site {
        display: inline-block;
        position: relative;
        margin-bottom: 36px;
        font-size: 18px;
        line-height: 22px;
        font-family: 'Gilroy-ExtraBold';
        &::before {
            content: '';
            position: absolute;
            bottom: -4px;
            left: 0;
            width: 100%;
            height: 2px;
            background-color: #DFA656;
        }
    }
    .developed-by {
        display: inline-flex;
        align-items: flex-start;
        align-self: flex-start;
        flex-direction: column;
        &__title {
            display: inline-block;
            margin-bottom: 8px;
        }
    }
    .social-list {
        display: flex;
        list-style-type: none;
        padding: 0;
        margin: 0 0 29px 0;
        &__item {
            margin-right: 28px;
            &:last-child {
                margin-right: 0;
            }
            a {
                display: flex;
            }
        }
    }
    @media (min-width: 1024px) {
        flex-direction: row;
        justify-content: space-between;
        padding: 43px 95px 68px 90px;
        .wrap-site-social-icons {
            display: flex;
        }
        .social-list {
            margin-bottom: 0;
            margin-left: 75px;
        }
        .copyright, .address, .site {
            font-size: 18px;
            line-height: 22px;
        }
        .copyright {
            margin-bottom: 20px;
        }
        .address {
            margin-bottom: 34px;
        }
        .site {
            margin-bottom: 8px;
        }
        .developed-by {
            align-self: flex-end;
        }
    }
    @media (min-width: 1920px) {
        padding: 54px 0 51px 0;
        .wrap {
            display: flex;
            width: 100%;
        }
        .copyright {
            margin-bottom: 0;
            margin-right: 110px;
        }
        .address {
            margin: 0 0 0 0;
        }
        .site {
            order: 2;
            margin-left: 130px;
            align-self: flex-end;
        }
        .wrap-copyright-address {
            display: flex;
            width: 55.1%;
            box-sizing: border-box;
            padding-left: 357px;
        }
        .wrap-site-social-icons {
            align-self: center;
            width: 44.9%;
            box-sizing: border-box;
            padding-left: 80px;
        }
        .social-list {
            order: 1;
            margin: 0;
        }
        .developed-by {
            margin-right: 64px;
        }
    }
`;