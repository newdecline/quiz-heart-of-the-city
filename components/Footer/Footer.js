import React from 'react';
import PinkChilliIcon from "../../svg/pinkchilli-logo.svg";
import {socialList} from '../../fakeData/socialList';
import {StyledFooter} from "./styled";

export const Footer = () => {
    return (
        <StyledFooter>
            <div className="wrap">
                <div className="wrap-copyright-address">
                    <div className="copyright">
                        © «Сердце города»<br/>
                        Все права защищены.
                    </div>
                    <div className="address">
                        109012, Москва, Новая пл. 6, 2 этаж <br/>
                        <a className='address-link' href="tel:88005506626">8 800 550 66 26</a>
                    </div>
                </div>
                <div className="wrap-site-social-icons">
                    <a className="site" target='_blank' href="https://xn--80afcbcb8danee7d.xn--p1ai">сердцегорода.рф</a>
                    <ul className="social-list">
                        {
                            socialList.map(item => (
                                <li key={item.href} className="social-list__item">
                                    <a rel='noreferrer noopener' target='_blank' href={item.href}>{item.icon}</a>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
            <a rel='noreferrer noopener' href='https://pinkchilli.agency' target='_blank' className="developed-by">
                <span className="developed-by__title">Разработка</span>
                <PinkChilliIcon/>
            </a>
        </StyledFooter>
    )
};