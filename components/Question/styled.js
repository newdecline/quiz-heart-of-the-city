import styled from "styled-components";

export const StyledQuestion = styled('div')`
    display: ${({isShow}) => isShow ? 'flex' : 'none'};
    flex: 1;
    flex-direction: column;
    padding-bottom: 17px;
    box-sizing: border-box;
    .question {
        &-header {
            padding: 29px 15px 11px 21px;
        }
        &-counter {
            margin-bottom: 9px;
            font-size: 12px;
            line-height: 14px;
            color: #dfa656;
        }
        &-title {
            margin-bottom: 3px;
            color: #dfa656;
            font-size: 14px;
            line-height: 16px;
        }
        &-hint {
            font-size: 12px;
            line-height: 14px;
            color: #c4c4c4;
        }
    }
    .list-answers {
        padding: 0 15px 0 21px;
        margin: 0 0 auto 0;
        list-style-type: none;
        box-sizing: border-box;
        &__item {
            margin-bottom: 17px;
            &:last-child {
                margin-bottom: 0;
            }
            &.selected {
                .text::before {
                    background-color: #ccc;
                }
            }
        }
    }
    .question-footer-buttons {
        padding: 0 16px 0 21px;
        margin: 17px 0 0 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .next-step {
        padding: 6px 28px;
        font-size: 14px;
        line-height: 17px;
        letter-spacing: 0.1em;
        font-family: 'Gilroy-ExtraBold';
        text-transform: uppercase;
        color: #fff;
        background: #DFA656;
        border-radius: 1px;
        box-sizing: border-box;
        border: 1px solid transparent;
        transition: border .3s, background .3s, color .3s;
        &:disabled {
            background: transparent;
            border: 1px solid #DFA656;
            color: #DFA656;
        }
    }
    .back-step {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        padding: 0;
        font-size: 12px;
        line-height: 14px;
        letter-spacing: 0.1em;
        text-transform: uppercase;
        background: transparent;
        border-radius: 1px;
        box-sizing: border-box;
        border: none;
        color: #c4c4c4;
        svg {
            margin-right: 5px;
        }
    }
    .back-step__skip {
        margin-left: 27px;
    }
    @media (min-width: 1024px) {
        padding-bottom: 43px;
        .question {
            &-header {
                padding: 46px 15px 27px 73px;
            }
            &-counter {
                margin-bottom: 20px;
                font-size: 18px;
                line-height: 21px;
            }
            &-title {
                font-size: 24px;
                line-height: 28px;
            }
            &-hint {
                font-size: 14px;
                line-height: 16px;
            }
        }
        .list-answers {
            padding: 0 15px 0 73px;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            height: auto;
            max-height: 250px;
            &__item {
                width: 50%;
                margin-bottom: 15px;
            }
        }
        .question-footer-buttons {
            padding: 0 48px 0 73px;
            margin: 0;
        }
        .back-step {
            font-size: 18px;
            line-height: 21px;
            &__text {
                display: flex;
                align-items: center;
            }
            &:hover {
                cursor: pointer;
            }
        }
        .next-step {
            padding: 11px 81px;
            font-size: 24px;
            line-height: 29px;
            &:hover {
                cursor: pointer;
            }
        }
    }
`;

export const StyledCheckBox = styled('label')`
    display: flex;
    .text {
        position: relative;
        padding-left: 31px;
        font-size: 14px;
        line-height: 16px;
        color: #333;
        &::before {
            content: '';
            position: absolute;
            top: 1px;
            left: 0;
            display: block;
            width: 14px;
            height: 14px;
            background-color: transparent;
            border-radius: 50%;
            border: 2px solid #DFA656;
            box-sizing: border-box;
            transition: background-color .3s;
        }
    }
    @media (min-width: 1024px) {
        .text {
            font-size: 18px;
            line-height: 21px;
            &::before {
                top: 4px;
            }
            &:hover {
                cursor: pointer;
            }
        }
    }
`;