import React from 'react';
import uuidv4 from 'uuid/v4';
import {inject, observer} from "mobx-react";
import ArrowBackIcon from "../../svg/arrow-back.svg";
import {quizStore} from "../../stores/quizStore";
import {StyledCheckBox, StyledQuestion} from "./styled";

export const Question = inject('quizStore')(observer(props => {
    const {
        quizStore,
        quizStore: {
            activeQuestion,
            quiz
        },
        nextAnswer,
        prevAnswer,
        isShow
    } = props;

    const handleClickAnswerItem = (item) => {
        quizStore.setAnswer(item.id);
    };

    const handleClickSkipBtn = () => {
        nextAnswer();
        quizStore.setAnswersSkip(activeQuestion);
        if (!quiz[activeQuestion].isReachGoal) {
            ym(process.env.YMID, 'reachGoal', `skipQuestion${activeQuestion + 1}`);
            quizStore.setReachGoal(activeQuestion);
            //    вопрос пропущен - цель достигнута
        }
    };

    const isDisableNextBtn = () => {
        if (quiz[activeQuestion].canSkip) {
            return false;
        } else if (!quiz[activeQuestion].canChoose) {
            return !quizStore.numberOfSelectedAnswersInQuestion.length
        } else {
            return !quizStore.numberOfSelectedAnswersInQuestion.length || quizStore.numberOfSelectedAnswersInQuestion.length > quiz[activeQuestion].canChoose
        }
    };

    return (
        <StyledQuestion isShow={isShow}>
            <div className="question-header">
                <div className="question-counter">Вопрос {activeQuestion + 1} из {quiz.length}</div>
                <div className="question-title">{quiz[quizStore.activeQuestion].question}</div>
                <div className="question-hint">{quiz[quizStore.activeQuestion].hint}
                </div>
            </div>

            <ul className="list-answers">
                {
                    quiz[quizStore.activeQuestion].answers.map((item) => {
                        return (
                            <li
                                onClick={() => handleClickAnswerItem(item)}
                                key={uuidv4()}
                                className={item.isSelected ? "list-answers__item selected" : "list-answers__item"}>
                                <StyledCheckBox>
                                    <div className="text">{item.text}</div>
                                </StyledCheckBox>
                            </li>
                        )
                    })
                }
            </ul>

            <div className="question-footer-buttons">
                <button
                    disabled={activeQuestion === 0}
                    className="back-step"
                >
                    <span onClick={prevAnswer} className='back-step__text'><ArrowBackIcon/>Назад</span>
                    {quiz[activeQuestion].canSkip &&
                    <span onClick={handleClickSkipBtn} className="back-step__skip">Пропустить</span>}
                </button>
                <button
                    onClick={nextAnswer}
                    disabled={isDisableNextBtn()}
                    className="next-step">Далее
                </button>
            </div>
        </StyledQuestion>
    )
}));

